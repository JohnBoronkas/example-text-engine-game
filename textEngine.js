const hpweekendDisk = () => ({
    /**
     * Wishlist
     * - Go to hogsmead
     * - Wompping Willow and shreaking shack
     * - Chamber of Secrets
     * - Brew other potions
     * - Magic use
     * - Take the hogwarts express back to real london (use actual london map)
     * - "GO WAIT" while in aszkaban. have to wait 1000 times to be released. mess with save file and autoreload back to prison until they do it
     * - If players find Peeves, he throws random items from their inventory into the world. mess with save file and autoreload back to this point
     * - spells link that shows all available spells for the player to cast
     */
    house: undefined,
    potion: [],
    burnerOn: false,
    roomofrequirementcount: 0,
    roomId: 'houseSelection', // Set this to the ID of the room you want the player to start in.
    rooms: [
        {
            id: 'houseSelection',
            name: 'House Selection',
            desc: 'Select your house by touching it below.\n*(Death Eater is listed for the kind of people that admit to doing drugs on their tax forms and wonder how they get busted. Oh, hi Mike.)*\n*(After selecting a command, you may have to scroll up to see the beginning of the text.)*',
            exits: [
                {
                    dir: 'gryffindor',
                    id: 'gryffindorcommonroom'
                },
                {
                    dir: 'hufflepuff',
                    id: 'hufflepuffcommonroom'
                },
                {
                    dir: 'ravenclaw',
                    id: 'ravenclawcommonroom'
                },
                {
                    dir: 'slytherin',
                    id: 'slytherincommonroom'
                },
                {
                    dir: 'deatheater',
                    id: 'azkaban'
                }
            ],
            items: [
                {
                    name: 'debug item',
                    isTakeable: true
                },
                {
                    name: 'debug item 2',
                    isTakeable: true
                },
                {
                    name: 'some item',
                    isTakeable: true
                }
            ]
        },
        {
            id: 'gryffindorcommonroom',
            name: 'Gryffindor Common Room',
            onEnter: () => setHouse('gryffindor'),
            desc: `It's winter break and Hogwarts is practically empty. You just got back to your common room after drinking with your friends from various houses for the last *you-don't-know-which-way-is-up-anymore* amount of hours.\nYou finally sit down by the fire to get some alone time when you notice your owl nudge your leg with a **LETTER**\n(Try **SEARCH** to find the **LETTER** and use **LOOK AT** to read it!)*`,
            items: [
                {
                    name: 'letter',
                    desc: `**LOOK AT** the letter to read it!`,
                    isTakeable: true,
                    onLook() {
                        println(`It's a letter from Professor Snape.`);
                        println(`"Meet me in the Potions Classroom, I need you to brew a potion that is so far beneath me that it's *almost* beneath you. Come now, do not keep me waiting."`);
                        println(`I guess I should find my way to the Potions Classroom to see what he needs me to do..\n*(Use **MOVE** to move between rooms.)*`);
                    },
                },
                {
                    name: 'sword of gryffindor',
                    desc: 'The Sword of Gryffindor hangs proudly above the fireplace. I should leave it there. If I need it, it will come to me.'
                }
            ],
            exits: [
                {
                    dir: '7',
                    id: '7thfloorhallways'
                },
                {
                    dir: '2',
                    id: '2ndfloorhallways'
                }
            ]
        },
        {
            id: 'hufflepuffcommonroom',
            name: 'Hufflepuff Common Room',
            onEnter: () => setHouse('hufflepuff'),
            desc: `It's winter break and Hogwarts is practically empty. You just got back to your common room after drinking with your friends from various houses for the last *you-don't-know-which-way-is-up-anymore* amount of hours.\nYou finally sit down by the fire to get some alone time when you notice your owl nudge your leg with a **LETTER**\n(Try **SEARCH** to find the **LETTER** and use **LOOK AT** to read it!)*`,
            items: [
                {
                    name: 'letter',
                    desc: `**LOOK AT** the letter to read it!`,
                    isTakeable: true,
                    onLook() {
                        println(`It's a letter from Professor Snape.`);
                        println(`"Meet me in the Potions Classroom, I need you to brew a potion that is so far beneath me that it's *almost* beneath you. Come now, do not keep me waiting."`);
                        println(`I guess I should find my way to the Potions Classroom to see what he needs me to do..\n*(Use **MOVE** to move between rooms.)*`);
                    },
                }
            ],
            exits: [
                {
                    dir: 'exit',
                    id: 'kitchenhallway'
                }
            ]
        },
        {
            id: 'kitchenhallway',
            name: 'Kitchen Hallway',
            desc: `A clean and functional storage area that sits next to the kitchen. There is a SPEW poster on the wall with several elementary drawings of socks done in crayon hung up around it and a suspicious looking pear sitting on top of a trio of barrels.\nThe door to the kitchen is locked.`,
            onEnter: () => resetRoomOfRequirementCount(),
            exits: [
                {
                    dir: 'stairs',
                    id: 'stairs'
                },
                {
                    dir: 'commonroom',
                    id: 'hufflepuffcommonroom',
                    block: 'A suspicious looking pear sits on a trio of barrels.'
                }
            ],
            items: [
                {
                    name: 'pear',
                    desc: 'A suspicious looking pear sits on a trio of barrels.',
                    onLook: () => {
                        const h = getHouse();
                        if (h === 'hufflepuff') {
                            println("Oh, I could **GO COMMONROOM** from here by tickling it!");
                            const room = getRoom('kitchenhallway');
                            const exit = getExit('commonroom', room.exits);
                            if (exit.block) {
                                delete exit.block;
                            }
                        }
                    }
                }
            ]
        },
        {
            id: 'ravenclawcommonroom',
            name: 'Ravenclaw Common Room',
            onEnter: () => setHouse('ravenclaw'),
            desc: `It's winter break and Hogwarts is practically empty. You just got back to your common room after drinking with your friends from various houses for the last *you-don't-know-which-way-is-up-anymore* amount of hours.\nYou finally sit down by the fire to get some alone time when you notice your owl nudge your leg with a **LETTER**\n(Try **SEARCH** to find the **LETTER** and use **LOOK AT** to read it!)*`,
            items: [
                {
                    name: 'letter',
                    desc: `**LOOK AT** the letter to read it!`,
                    isTakeable: true,
                    onLook() {
                        println(`It's a letter from Professor Snape.`);
                        println(`"Meet me in the Potions Classroom, I need you to brew a potion that is so far beneath me that it's *almost* beneath you. Come now, do not keep me waiting."`);
                        println(`I guess I should find my way to the Potions Classroom to see what he needs me to do..\n*(Use **MOVE** to move between rooms.)*`);
                    },
                },
                {
                    name: 'pieces of the found diadem',
                    desc: 'The now found Diadem of Ravenclaw lays shattered in a display case in the corner of the common room.'
                }
            ],
            exits: [
                {
                    dir: 'exit',
                    id: '7thfloorhallways'
                }
            ]
        },
        {
            id: 'slytherincommonroom',
            name: 'Slytherin Common Room',
            onEnter: () => setHouse('slytherin'),
            desc: `It's winter break and Hogwarts is practically empty. You just got back to your common room after drinking with your friends from various houses for the last *you-don't-know-which-way-is-up-anymore* amount of hours.\nYou finally sit down by the fire to get some alone time when you notice your owl nudge your leg with a **LETTER**\n(Try **SEARCH** to find the **LETTER** and use **LOOK AT** to read it!)*`,
            items: [
                {
                    name: 'letter',
                    desc: `**LOOK AT** the letter to read it!`,
                    isTakeable: true,
                    onLook() {
                        println(`It's a letter from Professor Snape.`);
                        println(`"Meet me in the Potions Classroom, I need you to brew a potion that is so far beneath me that it's *almost* beneath you. Come now, do not keep me waiting."`);
                        println(`I guess I should find my way to the Potions Classroom to see what he needs me to do..\n*(Use **MOVE** to move between rooms.)*`);
                    },
                }
            ],
            exits: [
                {
                    dir: 'exit',
                    id: 'dungeonhallways'
                }
            ]
        },
        {
            id: 'dungeonhallways',
            name: 'Dungeon Hallways',
            desc: `The dark stone emits a chilling presence within the twisting hallways of the dungeons.`,
            onEnter: () => resetRoomOfRequirementCount(),
            exits: [
                {
                    dir: 'stairs',
                    id: 'stairs'
                },
                {
                    dir: 'office',
                    id: 'snapesoffice'
                },
                {
                    dir: 'potions',
                    id: 'potionsclassroom'
                }
            ]
        },
        {
            id: 'snapesoffice',
            name: "Snape's Office",
            desc: `The door was left unlocked.\nThe shadowy walls are lined with shelves of large glass jars, in which floated all manner of revolting things that I can **SEARCH**. The fireplace is dark and empty. The lily on his desk seems to be the only living thing in the office.`,
            items: [
                {
                    name: 'lily flower',
                    onTake: () => println('I know better than to try and take this.')
                },
                {
                    name: 'bicorn horn',
                    isTakeable: true,
                    onUse: () => {
                        const room = getRoom(disk.roomId);
                        if (room.id === 'potionsclassroom') {
                            println('You add the bicorn horn to the cauldron.');
                            addToPotion('bicorn horn');
                        }
                        else {
                            println("I need to be in the Potions Classroom to add this to the cauldron.");
                        }
                    }
                },
                {
                    name: 'unicorn hair',
                    isTakeable: true,
                    onUse: () => {
                        const room = getRoom(disk.roomId);
                        if (room.id === 'potionsclassroom') {
                            println('You add the unicorn hair to the cauldron.');
                            addToPotion('unicorn hair');
                        }
                        else {
                            println("I need to be in the Potions Classroom to add this to the cauldron.");
                        }
                    }
                },
                {
                    name: 'crushed snake fangs',
                    isTakeable: true,
                    onUse: () => {
                        const room = getRoom(disk.roomId);
                        if (room.id === 'potionsclassroom') {
                            println("You add the crushed snake fangs to the cauldron.");
                            addToPotion('crushed snake fangs');
                        }
                        else {
                            println("I need to be in the Potions Classroom to add this to the cauldron.");
                        }
                    }
                },
                {
                    name: 'gillyweed',
                    isTakeable: true,
                    onUse: () => {
                        const room = getRoom(disk.roomId);
                        if (room.id === 'potionsclassroom') {
                            println("You add the gillyweed to the cauldron.");
                            addToPotion('gillyweed');
                        }
                        else {
                            println("I need to be in the Potions Classroom to add this to the cauldron.");
                        }
                    }
                },
                {
                    name: 'billywig wings',
                    isTakeable: true,
                    onUse: () => {
                        const room = getRoom(disk.roomId);
                        if (room.id === 'potionsclassroom') {
                            println('You add the billywig wings to the cauldron.');
                            addToPotion('billywig wings');
                        }
                        else {
                            println("I need to be in the Potions Classroom to add this to the cauldron.");
                        }
                    }
                },
                {
                    name: 'boomslang skin',
                    isTakeable: true,
                    onUse: () => {
                        const room = getRoom(disk.roomId);
                        if (room.id === 'potionsclassroom') {
                            println("You add the boomslang skin to the cauldron.");
                            addToPotion('boomslang skin');
                        }
                        else {
                            println("I need to be in the Potions Classroom to add this to the cauldron.");
                        }
                    }
                },
                {
                    name: 'whole flobberworm',
                    isTakeable: true,
                    onUse: () => {
                        const room = getRoom(disk.roomId);
                        if (room.id === 'potionsclassroom') {
                            println("You add the whole flobberworm to the cauldron.");
                            addToPotion('whole flobberworm');
                        }
                        else {
                            println("I need to be in the Potions Classroom to add this to the cauldron.");
                        }
                    }
                },
                {
                    name: 'bottled hope',
                    isTakeable: true,
                    onUse: () => {
                        const room = getRoom(disk.roomId);
                        if (room.id === 'potionsclassroom') {
                            println("You add hope to the cauldron.");
                            addToPotion('hope');
                        }
                        else {
                            println("I need to be in the Potions Classroom to add this to the cauldron.");
                        }
                    }
                },
                {
                    name: 'a review of resurrection research by gilderoy lockhart',
                    isTakeable: true
                },
                {
                    name: 'shrivelfigs',
                    isTakeable: true,
                    onUse: () => {
                        const room = getRoom(disk.roomId);
                        if (room.id === 'potionsclassroom') {
                            println('You add the shrivelfigs to the cauldron.');
                            addToPotion('shrivelfigs');
                        }
                        else {
                            println("I need to be in the Potions Classroom to add this to the cauldron.");
                        }
                    }
                },
                {
                    name: 'morning dew',
                    isTakeable: true,
                    onUse: () => {
                        const room = getRoom(disk.roomId);
                        if (room.id === 'potionsclassroom') {
                            println("You add the morning dew to the cauldron.");
                            addToPotion('morning dew');
                        }
                        else {
                            println("I need to be in the Potions Classroom to add this to the cauldron.");
                        }
                    }
                },
                {
                    name: 'wrackspurts',
                    isTakeable: true,
                    onUse: () => {
                        const room = getRoom(disk.roomId);
                        if (room.id === 'potionsclassroom') {
                            println("You add the wrackspurts to the cauldron.");
                            addToPotion('wrackspurts');
                        }
                        else {
                            println("I need to be in the Potions Classroom to add this to the cauldron.");
                        }
                    }
                }
            ],
            exits: [
                {
                    dir: 'exit',
                    id: 'dungeonhallways'
                }
            ]
        },
        {
            id: 'potionsclassroom',
            name: 'Potions Classroom',
            desc: `Professor Snape is here. He's the Potions Master and Head of Slytherin house at Hogwarts. He has sallow skin, greasy hair, a hooked nose, black eyes and a severe dislike of Harry Potter.\nProfessor Snape says "Finally. The boy fool Longbottom got himself boils for trying to order off of the secret menu while underage. Help me gather the ingredients for the cure and ***I'll unlock the secret menu for you as a reward***.\nListen carefully.\nYou'll need crushed snake fangs, sliced pungous onions, dried nettles, flobberworm mucus, powdered ginger root, pickled shrake spines, stewed horned slugs, and porcupine quills.\nThe pantry in here has some things, but you'll need to go see Hagrid at his hut down the hill and Professor Sprout in the Northern courtyard greenhouse for the others. You may use my personal ingredient store in my office if you must, but I must caution you to use intense care in what you choose to look at and touch.\nYour safety is not guaranteed.\nOnce you have gathered everything, return here to this cauldron and **USE** the ingredients and your *Book of Potions* to brew a *Cure for Boils* potion."\n(Remember to **SAVE** your game before attempting to brew the potion and to **LOAD** when you mess up).`,
            exits: [
                {
                    dir: 'pantry',
                    id: 'pantry'
                },
                {
                    dir: 'exit',
                    id: 'dungeonhallways'
                }
            ],
            items: [
                {
                    name: 'cauldron burner',
                    desc: `The burner under the cauldron. I can **USE** it to turn it on and off.`,
                    onUse: () => {
                        const burnerState = toggleBurner();
                        println(`You toggle the cauldron burner. The ${burnerState}.`);
                        addToPotion(burnerState);
                    },
                    onTake: () => println(`This is too heavy to take.`)
                },
                {
                    name: 'gentle stirring spoon',
                    desc: `A beautiful silver spoon labeled with "For gentle stirring ONLY".`,
                    onUse: () => {
                        println('You stir gently.');
                        addToPotion('gentle stir');
                    },
                    onTake: () => println(`The professor wouldn't like me taking his spoon.`)
                },
                {
                    name: 'vigorous stirring spoon',
                    desc: `A well-worn and chipped wooden spoon labeled with "For vigorous stirring ONLY".`,
                    onUse: () => {
                        println('You stir with intense vigor.');
                        addToPotion('vigorous stir');
                    },
                    onTake: () => println(`The professor wouldn't like me taking his spoon.`)
                }
            ]
        },
        {
            id: 'pantry',
            name: 'Pantry',
            desc: `The small room is filled with various shelves, drawers, and hanging herbs. It seems to be low on ingredients due to the end of term. I can **SEARCH** for ingredients in here.`,
            onEnter: () => {
                const potionsroom = getRoom('potionsclassroom');
                potionsroom.desc = `Professor Snape is here, but I shouldn't bother him anymore. He told me to brew a *Cure for Boils* potion using the *Book of Potions* to unlock the secret menu.\nI can **USE** ingredients to add them to the cauldron, **SEARCH** will find useful tools that I may need to **USE**, and of course I have my **WAND** on me to complete the recipe. I need to be careful to do them in the right order.\n(Remember to **SAVE** your game before attempting to brew the potion and to **LOAD** when you mess up).`
            },
            exits: [
                {
                    dir: 'exit',
                    id: 'potionsclassroom'
                }
            ],
            items: [
                {
                    name: 'dried nettles',
                    isTakeable: true,
                    onUse: () => {
                        const room = getRoom(disk.roomId);
                        if (room.id === 'potionsclassroom') {
                            println('You add the dried nettles to the cauldron.');
                            addToPotion('dried nettles');
                        }
                        else {
                            println("I need to be in the Potions Classroom to add this to the cauldron.");
                        }
                    }
                },
                {
                    name: 'cowbane essence',
                    isTakeable: true,
                    onUse: () => {
                        const room = getRoom(disk.roomId);
                        if (room.id === 'potionsclassroom') {
                            println('You add the cowbane essence to the cauldron.');
                            addToPotion('cowbane essence');
                        }
                        else {
                            println("I need to be in the Potions Classroom to add this to the cauldron.");
                        }
                    }
                },
                {
                    name: 'ladys mantle (dried)',
                    isTakeable: true,
                    onUse: () => {
                        const room = getRoom(disk.roomId);
                        if (room.id === 'potionsclassroom') {
                            println('You add the dried ladys mantle to the cauldron.');
                            addToPotion('dried ladys mantle');
                        }
                        else {
                            println("I need to be in the Potions Classroom to add this to the cauldron.");
                        }
                    }
                },
                {
                    name: 'porcupine quills',
                    isTakeable: true,
                    onUse: () => {
                        const room = getRoom(disk.roomId);
                        if (room.id === 'potionsclassroom') {
                            println('You add the porcupine quills to the cauldron.');
                            addToPotion('porcupine quills');
                        }
                        else {
                            println("I need to be in the Potions Classroom to add this to the cauldron.");
                        }
                    }
                },
                {
                    name: 'knarl quills',
                    isTakeable: true,
                    onUse: () => {
                        const room = getRoom(disk.roomId);
                        if (room.id === 'potionsclassroom') {
                            println('You add the knarl quills to the cauldron.');
                            addToPotion('knarl quills');
                        }
                        else {
                            println("I need to be in the Potions Classroom to add this to the cauldron.");
                        }
                    }
                },
                {
                    name: 'hemlock essence',
                    isTakeable: true,
                    onUse: () => {
                        const room = getRoom(disk.roomId);
                        if (room.id === 'potionsclassroom') {
                            println('You add the hemlock essence to the cauldron.');
                            addToPotion('hemlock essence');
                        }
                        else {
                            println("I need to be in the Potions Classroom to add this to the cauldron.");
                        }
                    }
                },
                {
                    name: 'tormentil tincture',
                    isTakeable: true,
                    onUse: () => {
                        const room = getRoom(disk.roomId);
                        if (room.id === 'potionsclassroom') {
                            println('You add the tormentil tincture to the cauldron.');
                            addToPotion('tormentil tincture');
                        }
                        else {
                            println("I need to be in the Potions Classroom to add this to the cauldron.");
                        }
                    }
                },
                {
                    name: 'common rue',
                    isTakeable: true,
                    onUse: () => {
                        const room = getRoom(disk.roomId);
                        if (room.id === 'potionsclassroom') {
                            println('You add the common rue to the cauldron.');
                            addToPotion('common rue');
                        }
                        else {
                            println("I need to be in the Potions Classroom to add this to the cauldron.");
                        }
                    }
                },
                {
                    name: 'leeches',
                    isTakeable: true,
                    onUse: () => {
                        const room = getRoom(disk.roomId);
                        if (room.id === 'potionsclassroom') {
                            println('You add the leeches to the cauldron.');
                            addToPotion('leeches');
                        }
                        else {
                            println("I need to be in the Potions Classroom to add this to the cauldron.");
                        }
                    }
                },
                {
                    name: 'caterpillars',
                    isTakeable: true,
                    onUse: () => {
                        const room = getRoom(disk.roomId);
                        if (room.id === 'potionsclassroom') {
                            println('You add the caterpillars to the cauldron.');
                            addToPotion('caterpillars');
                        }
                        else {
                            println("I need to be in the Potions Classroom to add this to the cauldron.");
                        }
                    }
                }
            ]
        },
        {
            id: 'azkaban',
            name: 'Azkaban',
            onEnter: () => {
                setHouse('deatheater');
                deleteItem('wand');
            },
            desc: `By the scant, flickering torchlight in the corridor, the confines of your cell are barely visible. Moisture glistens on the dark stone walls, radiating the cold. Your teeth chatter together as you pull your thin cloak around your shoulders.\nDreams of escape light up your mind for a brief moment before fading. The thought slips away, despite all of your efforts to hold onto it. Happiness of any form is a mere memory, and whatever is left does not linger long in this wretched place.\nEscape is impossible.\nEscape is impossible.\nEscape is impossible.\nThe door to your cell is heavy, carved from dark ironwood set into steel brackets. A small slot lay at the bottom of the door, where your meager meals of molded bread and cloudy water are passed through three times a day. After Sirius' escape, it had been decided that opening the door to feed a prisoner was too dangerous.\nThe "window" is a two-foot wide aperture, crisscrossed with rusted iron bars that overlook the violent grey waves that crash against the side of the cliffs far below your cell's tower. Icy wind pierces your thin cloak with a light mist from the heavy rain.\nEscape is impossible.\nEscape is impossible.\nEscape is impossible.\nYour choices have consequences. It has been proven that your free will is too much for you to bear. A dementor will soon relieve you of your responsibilities for an eternity.\nEscape is impossible.\nEscape is impossible.\nEscape is impossible.\n*(Adapted from Elizium for the Sleepless Souls, Chapter I: Seasons in the Ice Cage, by Voice of the Nephilim)*`
        },
        {
            id: 'stairs',
            name: 'Stairs',
            desc: `You're on the stairs with access to all levels of Hogwarts. No swiveling crap to deal with this year.`,
            exits: [
                {
                    dir: '7',
                    id: '7thfloorhallways'
                },
                {
                    dir: '6',
                    id: '6thfloorhallways'
                },
                {
                    dir: '5',
                    id: '5thfloorhallways'
                },
                {
                    dir: '4',
                    id: '4thfloorlibrary'
                },
                {
                    dir: '3',
                    id: '3rdfloorhallways'
                },
                {
                    dir: '2',
                    id: '2ndfloorhallways'
                },
                {
                    dir: 'main',
                    id: 'entrancehall'
                },
                {
                    dir: 'kitchen',
                    id: 'kitchenhallway'
                },
                {
                    dir: 'dungeon',
                    id: 'dungeonhallways'
                }
            ]
        },
        {
            id: 'entrancehall',
            name: 'Entrance Hall',
            desc: `You stand in the middle of the massive main entrance to Hogwarts. The staff room and Filch's Office are locked. Gargoyles guard the entrance to the Staff Room, I know better than to try it.`,
            onEnter: () => resetRoomOfRequirementCount(),
            exits: [
                {
                    dir: 'stairs',
                    id: 'stairs'
                },
                {
                    dir: 'greathall',
                    id: 'greathall'
                },
                {
                    dir: 'outside',
                    id: 'centralcourtyard'
                }
            ]
        },
        {
            id: 'greathall',
            name: 'Great Hall',
            desc: `The great hall is closed for the night, you seem to be the only one here. It's too dark to see the house points display that's probably covered for the break, but maybe you know a spell that can produce light?`,
            exits: [
                {
                    dir: 'exit',
                    id: 'entrancehall'
                }
            ]
        },
        {
            id: 'centralcourtyard',
            name: 'Central Courtyard',
            desc: `In the shadow of the entrance hall, you can see your breath in the cool air. You reflexively pull your cloak around you a little tighter. You're in the middle of the courtyard that extends to the north and south. Down the hill, you can see Hagrid's Hut. There's a path that leads to the Quidditch Pitch.`,
            exits: [
                {
                    dir: 'entrance',
                    id: 'entrancehall'
                },
                {
                    dir: 'north',
                    id: 'northerncourtyard'
                },
                {
                    dir: 'south',
                    id: 'sotherncourtyard'
                },
                {
                    dir: 'hut',
                    id: 'hagridshut'
                },
                {
                    dir: 'pitch',
                    id: 'quidditchpitch'
                }
            ]
        },
        {
            id: 'quidditchpitch',
            name: 'Quidditch Pitch',
            desc: `The pitch and stands are empty. There's a broom laying next to the entrance to the grandstands and a small gold glint races through the sky in excited spins and drops.\nI can follow the path back to the Central Courtyard or fly somewhere with the broom.`,
            items: [
                {
                    name: 'snitch',
                    onTake: () => println(`I'm too slow to catch the snitch.`)
                }
            ],
            exits: [
                {
                    dir: 'back',
                    id: 'centralcourtyard'
                },
                {
                    dir: '7',
                    id: '7thfloorhallways'
                },
                {
                    dir: 'north',
                    id: 'northerncourtyard'
                },
                {
                    dir: 'center',
                    id: 'centralcourtyard'
                },
                {
                    dir: 'south',
                    id: 'southerncourtyard'
                },
                {
                    dir: 'hut',
                    id: 'hagridshut'
                },
                {
                    dir: 'azkaban',
                    id: 'azkaban'
                },
                {
                    dir: 'forest',
                    id: 'forbiddenforest'
                }
            ]
        },
        {
            id: 'southerncourtyard',
            name: 'Southern Courtyard',
            desc: `The southern courtyard is empty. There is nothing of interest here. Why did you come here?`,
            exits: [
                {
                    dir: 'north',
                    id: 'centralcourtyard'
                }
            ]
        },
        {
            id: 'northerncourtyard',
            name: 'Northern Courtyard',
            desc: `The garden gate is barren for the cold months. The greenhouse nearby is overflowing with plants sheltering for the winter.`,
            exits: [
                {
                    dir: 'south',
                    id: 'centralcourtyard'
                },
                {
                    dir: 'greenhouse',
                    id: 'greenhouse'
                }
            ]
        },
        {
            id: 'greenhouse',
            name: 'Green House',
            desc: `You have to take off your cloak from the unnatural amount of heat in here. The fumes of fertilizer and the mixture of the plants' heavy air assault your senses. It's hard to even decern the main walkways within the greenhouse. The plant life is crowding any available space causing you to feel claustrophobic. Professor Sprout doesn't seem to be here, leaving you to **SEARCH** for and **TAKE** what you need on your own.`,
            exits: [
                {
                    dir: 'exit',
                    id: 'northerncourtyard'
                }
            ],
            items: [
                {
                    name: 'wormwood',
                    isTakeable: true,
                    onUse: () => {
                        const room = getRoom(disk.roomId);
                        if (room.id === 'potionsclassroom') {
                            println('You add the wormwood to the cauldron.');
                            addToPotion('wormwood');
                        }
                        else {
                            println("I need to be in the Potions Classroom to add this to the cauldron.");
                        }
                    }
                },
                {
                    name: 'daisy roots',
                    isTakeable: true,
                    onUse: () => {
                        const room = getRoom(disk.roomId);
                        if (room.id === 'potionsclassroom') {
                            println('You add the daisy roots to the cauldron.');
                            addToPotion('daisy roots');
                        }
                        else {
                            println("I need to be in the Potions Classroom to add this to the cauldron.");
                        }
                    }
                },
                {
                    name: 'knotgrass',
                    isTakeable: true,
                    onUse: () => {
                        const room = getRoom(disk.roomId);
                        if (room.id === 'potionsclassroom') {
                            println('You add the knotgrass to the cauldron.');
                            addToPotion('knotgrass');
                        }
                        else {
                            println("I need to be in the Potions Classroom to add this to the cauldron.");
                        }
                    }
                },
                {
                    name: 'bundimun',
                    isTakeable: true,
                    onUse: () => {
                        const room = getRoom(disk.roomId);
                        if (room.id === 'potionsclassroom') {
                            println('You add the bundimun acid to the cauldron.');
                            addToPotion('bundimun');
                        }
                        else {
                            println("I need to be in the Potions Classroom to add this to the cauldron.");
                        }
                    }
                },
                {
                    name: 'powdered ginger root',
                    isTakeable: true,
                    onUse: () => {
                        const room = getRoom(disk.roomId);
                        if (room.id === 'potionsclassroom') {
                            println('You add a sprinkle of powdered ginger root to the cauldron.');
                            addToPotion('powdered ginger root');
                        }
                        else {
                            println("I need to be in the Potions Classroom to add this to the cauldron.");
                        }
                    }
                },
                {
                    name: 'squill bulb',
                    isTakeable: true,
                    onUse: () => {
                        const room = getRoom(disk.roomId);
                        if (room.id === 'potionsclassroom') {
                            println('You add the squill bulb to the cauldron.');
                            addToPotion('squill bulb');
                        }
                        else {
                            println("I need to be in the Potions Classroom to add this to the cauldron.");
                        }
                    }
                },
                {
                    name: 'alihotsy leaves',
                    isTakeable: true,
                    onUse: () => {
                        const room = getRoom(disk.roomId);
                        if (room.id === 'potionsclassroom') {
                            println('You add the alihotsy leaves to the cauldron.');
                            addToPotion('alihotsy leaves');
                        }
                        else {
                            println("I need to be in the Potions Classroom to add this to the cauldron.");
                        }
                    }
                },
                {
                    name: 'sliced pungous onions',
                    isTakeable: true,
                    onUse: () => {
                        const room = getRoom(disk.roomId);
                        if (room.id === 'potionsclassroom') {
                            println('You add the sliced pungous onions to the cauldron.');
                            addToPotion('sliced pungous onions');
                        }
                        else {
                            println("I need to be in the Potions Classroom to add this to the cauldron.");
                        }
                    }
                },
                {
                    name: 'fluxweed',
                    isTakeable: true,
                    onUse: () => {
                        const room = getRoom(disk.roomId);
                        if (room.id === 'potionsclassroom') {
                            println('You add the fluxweed to the cauldron.');
                            addToPotion('fluxweed');
                        }
                        else {
                            println("I need to be in the Potions Classroom to add this to the cauldron.");
                        }
                    }
                },
                {
                    name: 'fresh rose',
                    isTakeable: true,
                    onUse: () => {
                        const room = getRoom(disk.roomId);
                        if (room.id === 'potionsclassroom') {
                            println('You add petals from the fresh rose to the cauldron.');
                            addToPotion('fresh rose');
                        }
                        else {
                            println("I need to be in the Potions Classroom to add this to the cauldron.");
                        }
                    }
                }
            ]
        },
        {
            id: 'hagridshut',
            name: "Hagrid's Hut",
            desc: `Hagrid's door seems to be.. missing? There is a charred outline around the outside of the door frame.\nHe is no where to be found and neither is Fang. They must be in the Forbidden Forest, I'm sure he won't mind me taking what I need. I'll just need to search for them myself.\nOh, there's a note hanging next to where the door *would be*. I could **SEARCH** and **LOOK AT** the **NOTE** to read it.`,
            items: [
                {
                    name: 'note',
                    desc: `"UPS Delivery Person,\nPlease do not deliver Blast-Ended Skrewts with their cage openings facing my VERY FLAMMABLE hut.\nRespectively,\nRubeus Hagrid"`,
                    onTake: () => println('I think I should leave this reminder here.')
                },
                {
                    name: 'puffskein',
                    desc: `The puffskein is sleeping on the table inside of a used cup. It is making a low and happy sounding humming sound as it rests. It has a small tag clipped to it that reads 'Misses Flooffles'.`,
                    isTakeable: true,
                    onUse: () => {
                        const room = getRoom(disk.roomId);
                        if (room.id === 'potionsclassroom') {
                            println(`You add Hagrid's VERY MUCH ALIVE puffskein named Misses Flooffles to the cauldron. You monster. Professor Snape looks equally amused and offended.`);
                            addToPotion('puffskein');
                        }
                        else {
                            println("I need to be in the Potions Classroom to add this to the cauldron.");
                        }
                    }
                }
            ],
            exits: [
                {
                    dir: 'exit',
                    id: 'centralcourtyard'
                },
                {
                    dir: 'drawers',
                    id: 'drawers'
                },
                {
                    dir: 'underbed',
                    id: 'underbed'
                },
                {
                    dir: 'cupboards',
                    id: 'cupboards'
                },
                {
                    dir: 'barrels',
                    id: 'barrels'
                },
                {
                    dir: 'forest',
                    id: 'forbiddenforest'
                }
            ]
        },
        {
            id: 'forbiddenforest',
            name: 'Forbidden Forest',
            desc: `Tree roots and stumps litter the forest floor as your cloak catches on low-slung branches and brambles. Knotgrass, throns, and branches cover the path as you travel deeper. If you turn back, you may be able to find your way back to Hagrid's.`,
            exits: [
                {
                    dir: 'back',
                    id: 'hagridshut'
                },
                {
                    dir: 'deeper',
                    id: 'forbiddenforest2'
                }
            ]
        },
        {
            id: 'forbiddenforest2',
            name: 'Forbidden Forest',
            desc: `The path continues to narrow and twist deeper into the abyss. Whatever light was left fades into the void. A cool wind rustles the trees and bushes.\nYou catch the glint of eyes following your every move.`,
            exits: [
                {
                    dir: 'back',
                    id: 'forbiddenforest'
                },
                {
                    dir: 'deeper',
                    id: 'forbiddenforest3'
                }
            ]
        },
        {
            id: 'forbiddenforest3',
            name: 'Forbidden Forest',
            desc: `The path is completely gone. Your legs scrape against the overgrowth. You need to take large steps to get over the cluttered forest floor. You're cut and bleeding. You frequently misstep and fall, futher cutting and bruising your body.`,
            exits: [
                {
                    dir: 'back',
                    id: 'forbiddenforest4'
                },
                {
                    dir: 'deeper',
                    id: 'forbiddenforest4'
                }
            ]
        },
        {
            id: 'forbiddenforest4',
            name: 'Forbidden Forest',
            desc: `The path is completely gone. Were you here already before? How long has it been? The bushes around you as you hear the low snarls of wildlife. They are waiting.\nThe heavy darkness encloses around you. The feeling of hope fading. Will you ever make it home?`,
            exits: [
                {
                    dir: 'back',
                    id: 'forbiddenforest5'
                },
                {
                    dir: 'deeper',
                    id: 'forbiddenforest5'
                }
            ]
        },
        {
            id: 'forbiddenforest5',
            name: 'Forbidden Forest',
            desc: `Your mind races as memories from all the teachers of the school warning of the dangers of the Forbidden Forest. Why didn't you listen? Forbidden is right in the name. All of those years of drilling it into you had no effect.`,
            exits: [
                {
                    dir: 'back',
                    id: 'forbiddenforest6'
                },
                {
                    dir: 'deeper',
                    id: 'forbiddenforest6'
                }
            ]
        },
        {
            id: 'forbiddenforest6',
            name: 'Forbidden Forest',
            desc: `Will anyone remember you? No one knows you entered the forest. Those animals are following you. Your body hurts. You feel exhausted. You don't know how long it's been since you got here. Hope is fading and the terror is truly starting to sink in.`,
            exits: [
                {
                    dir: 'back',
                    id: 'forbiddenforest7'
                },
                {
                    dir: 'deeper',
                    id: 'forbiddenforest7'
                }
            ]
        },
        {
            id: 'forbiddenforest7',
            name: 'Forbidden Forest',
            desc: `Did you remember to **SAVE** your game? Or did you choose to ignore those warnings too? Given you gotten yourself into this predicament, I wouldn't be suprised if you had no **SAVE** to **LOAD** and you had to restart. Maybe this is the lesson you needed.`,
            exits: [
                {
                    dir: 'back',
                    id: 'forbiddenforest8'
                },
                {
                    dir: 'deeper',
                    id: 'forbiddenforest8'
                }
            ]
        },
        {
            id: 'forbiddenforest8',
            name: 'Forbidden Forest',
            desc: `You start murmuring to yourself..`,
            exits: [
                {
                    dir: 'back',
                    id: 'forbiddenforest9'
                },
                {
                    dir: 'deeper',
                    id: 'forbiddenforest9'
                }
            ]
        },
        {
            id: 'forbiddenforest9',
            name: 'Forbidden Forest',
            desc: `THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVERTHE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVERTHE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVERTHE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVERTHE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVERTHE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVERTHE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVERTHE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVERTHE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVERTHE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVERTHE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVERTHE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVERTHE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVERTHE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVERTHE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVERTHE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVERTHE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVERTHE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVERTHE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVERTHE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVERTHE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVERTHE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVERTHE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVERTHE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVERTHE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVERTHE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVERTHE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVERTHE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVERTHE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVERTHE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVERTHE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVERTHE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVERTHE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVERTHE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVERTHE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVERTHE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVERTHE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVERTHE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER THE END IS NEVER **THE END IS NEVER THE END** IS NEVER THE END IS NEVER THE END IS NEVER`,
            exits: [
                {
                    dir: 'back',
                    id: 'forbiddenforest4'
                },
                {
                    dir: 'deeper',
                    id: 'forbiddenforest10'
                }
            ]
        },
        {
            id: 'forbiddenforest10',
            name: 'Forbidden Forest',
            desc: `You trip and fall into a half-inch stream of water.\n**GLUG GLUG**.\nThe end.\nWhat an embarrassing way to die.\nPut down the phone and take a moment to reflect on your life and how you got to this very moment.`,
        },
        {
            id: 'drawers',
            name: "Hagrid's Drawers",
            desc: `Drawers near Hagrid's bed, probably full of his personal things, but if I'm quick, it might be worth a **SEARCH**.`,
            exits: [
                {
                    dir: 'back',
                    id: 'hagridshut'
                }
            ],
            items: [
                {
                    name: 'theres nothing here, I wonder where he went since he took all of his personal items with him'
                }
            ]
        },
        {
            id: 'underbed',
            name: "Under Hagrid's Bed",
            desc: `If anyone has monsters under their bed, it's Hagrid. *You instintively draw your wand before you **SEARCH***.`,
            exits: [
                {
                    dir: 'back',
                    id: 'hagridshut'
                }
            ],
            items: [
                {
                    name: 'lacewing flies',
                    isTakeable: true,
                    onUse: () => {
                        const room = getRoom(disk.roomId);
                        if (room.id === 'potionsclassroom') {
                            println('You add the lacewing flies to the cauldron.');
                            addToPotion('lacewing flies');
                        }
                        else {
                            println("I need to be in the Potions Classroom to add this to the cauldron.");
                        }
                    }
                },
                {
                    name: 'occamy eggshells',
                    isTakeable: true,
                    onUse: () => {
                        const room = getRoom(disk.roomId);
                        if (room.id === 'potionsclassroom') {
                            println('You add the occamy eggshells to the cauldron.');
                            addToPotion('occamy eggshells');
                        }
                        else {
                            println("I need to be in the Potions Classroom to add this to the cauldron.");
                        }
                    }
                },
                {
                    name: 'streeler shells',
                    isTakeable: true,
                    onUse: () => {
                        const room = getRoom(disk.roomId);
                        if (room.id === 'potionsclassroom') {
                            println('You add the streeler shells to the cauldron.');
                            addToPotion('streeler shells');
                        }
                        else {
                            println("I need to be in the Potions Classroom to add this to the cauldron.");
                        }
                    }
                },
                {
                    name: 'stewed horned slugs',
                    isTakeable: true,
                    onUse: () => {
                        const room = getRoom(disk.roomId);
                        if (room.id === 'potionsclassroom') {
                            println('You add a glug of stewed horned slugs to the cauldron.');
                            addToPotion('stewed horned slugs');
                        }
                        else {
                            println("I need to be in the Potions Classroom to add this to the cauldron.");
                        }
                    }
                },
                {
                    name: 'murtlap anemone-like growth',
                    isTakeable: true,
                    onUse: () => {
                        const room = getRoom(disk.roomId);
                        if (room.id === 'potionsclassroom') {
                            println('You add the murtlap growth to the cauldron.');
                            addToPotion('murtlap growth');
                        }
                        else {
                            println("I need to be in the Potions Classroom to add this to the cauldron.");
                        }
                    }
                },
                {
                    name: 'bound book of monsters',
                    desc: `The book is bound to the floor with heavy chains, I'm going to leave it here.`
                }
            ]
        },
        {
            id: 'barrels',
            name: "Barrels near the Fireplace",
            desc: `There are two open barrels near Hagrid's fireplace. The embers give off some heat, but it is no match for the rigid cold penetrating the room through the missing door.`,
            items: [
                {
                    name: 'firewood',
                    desc: "It's wood. Shocking, right?",
                    onTake: () => println('I should leave this here, he needs it more than I do.')
                },
                {
                    name: 'poker',
                    desc: "A well chared rod of iron. A little bent out of shape with a cracked wooden handle, but easily functional. Might be a good Christmas gift for him this year. Or, could I just repair this one?",
                    onTake: () => println(`I don't have a use for this.`)
                }
            ],
            exits: [
                {
                    dir: 'back',
                    id: 'hagridshut'
                }
            ]
        },
        {
            id: 'cupboards',
            name: "Hagrid's Cupboards",
            desc: `There is a lot in here, I should **SEARCH** and **TAKE** what I need.`,
            exits: [
                {
                    dir: 'back',
                    id: 'hagridshut'
                }
            ],
            items: [
                {
                    name: 'dragon liver',
                    isTakeable: true,
                    onUse: () => {
                        const room = getRoom(disk.roomId);
                        if (room.id === 'potionsclassroom') {
                            println('You add the dragon liver to the cauldron.');
                            addToPotion('dragon liver');
                        }
                        else {
                            println("I need to be in the Potions Classroom to add this to the cauldron.");
                        }
                    }
                },
                {
                    name: 'thyme',
                    isTakeable: true,
                    onUse: () => {
                        const room = getRoom(disk.roomId);
                        if (room.id === 'potionsclassroom') {
                            println('You add the thyme to the cauldron.');
                            addToPotion('thyme');
                        }
                        else {
                            println("I need to be in the Potions Classroom to add this to the cauldron.");
                        }
                    }
                },
                {
                    name: 'ashwinder egg',
                    isTakeable: true,
                    onUse: () => {
                        const room = getRoom(disk.roomId);
                        if (room.id === 'potionsclassroom') {
                            println('You add the ashwinder egg to the cauldron.');
                            addToPotion('ashwinder egg');
                        }
                        else {
                            println("I need to be in the Potions Classroom to add this to the cauldron.");
                        }
                    }
                },
                {
                    name: 'flobberworm mucus',
                    isTakeable: true,
                    onUse: () => {
                        const room = getRoom(disk.roomId);
                        if (room.id === 'potionsclassroom') {
                            println('You add a dash of flobberworm mucus to the cauldron.');
                            addToPotion('flobberworm mucus');
                        }
                        else {
                            println("I need to be in the Potions Classroom to add this to the cauldron.");
                        }
                    }
                },
                {
                    name: 'pickled shrake spines',
                    isTakeable: true,
                    onUse: () => {
                        const room = getRoom(disk.roomId);
                        if (room.id === 'potionsclassroom') {
                            println('You add the pickled shrake spines to the cauldron.');
                            addToPotion('pickled shrake spines');
                        }
                        else {
                            println("I need to be in the Potions Classroom to add this to the cauldron.");
                        }
                    }
                },
                {
                    name: 'horseradish',
                    isTakeable: true,
                    onUse: () => {
                        const room = getRoom(disk.roomId);
                        if (room.id === 'potionsclassroom') {
                            println('You add the horseradish to the cauldron.');
                            addToPotion('horseradish');
                        }
                        else {
                            println("I need to be in the Potions Classroom to add this to the cauldron.");
                        }
                    }
                }
            ]
        },
        {
            id: '2ndfloorhallways',
            name: '2nd Floor Hallways',
            desc: `There aren't screams coming from the Hospital Wing during the off-season which is a nice change of pace. There's a **TAPESTRY** that spans from floor to ceiling near the locked Defense Against the Dark Arts Office.\nWeird, the girl's bathroom is *still* out of order.`,
            items: [
                {
                    name: 'tapestry',
                    desc: "A tapestry that's probably beautiful, but no one knows what it looks like because it isn't described. Thanks, JKR.",
                    onLook: () => {
                        if (getHouse() === 'gryffindor') {
                            println("Oh, this is where the secret passage is up to our common room is, I could **GO COMMONROOM** from here!");
                            const room = getRoom('2ndfloorhallways');
                            const exit = getExit('commonroom', room.exits);
                            if (exit.block) {
                                delete exit.block;
                            }
                        }
                    }
                }
            ],
            onEnter: () => resetRoomOfRequirementCount(),
            exits: [
                {
                    dir: 'stairs',
                    id: 'stairs'
                },
                {
                    dir: 'hospital',
                    id: 'hospital'
                },
                {
                    dir: 'commonroom',
                    id: 'gryffindorcommonroom',
                    block: "A tapestry that's probably beautiful, but no one knows what it looks like because it isn't described. Thanks, JKR. I could try **LOOK AT TAPESTRY** to see if there is anything remarkable about it."
                }
            ]
        },
        {
            id: 'hospital',
            name: 'Hospital Wing',
            desc: `This place isn't very clean for a hospital. Madam Pomfrey's Office is locked, she seems to be busy through the window. Neville is here, he is covered in boils, knocked out, and doesn't seem to be doing too well. I can **SEARCH** and **LOOK AT** his **CHARTS**. His **REMEMBRALL** is unattended on his nightstand.`,
            items: [
                {
                    name: 'remembrall',
                    isTakeable: true,
                    onLook: () => println('It faintly glows with red smoke inside.')
                },
                {
                    name: 'charts',
                    desc: `"Student forgot to turn off the heat before adding the porcupine quills while brewing a Cure for Boils potion."`,
                    onTake: () => println('I should leave these here as a reminder for him when he wakes up.')
                }
            ],
            exits: [
                {
                    dir: 'exit',
                    id: '2ndfloorhallways'
                }
            ]
        },
        {
            id: '3rdfloorhallways',
            name: '3rd Floor Hallways',
            desc: `This floor of classrooms is empty. It sounds like there is a gathering of ghosts behind one of the locked doors. I shouldn't disturb them to avoid pissing off Peeves.\nThere is a open trapdoor that seems to go down several levels, but I could safely jump down it. How is this legal..? A first year could get serously hurt.`,
            onEnter: () => resetRoomOfRequirementCount(),
            exits: [
                {
                    dir: 'stairs',
                    id: 'stairs'
                },
                {
                    dir: 'trapdoor',
                    id: 'dungeonhallways'
                }
            ]
        },
        {
            id: '4thfloorlibrary',
            name: '4th Floor Library',
            desc: `The library has limited hours during break and is currently closed.`,
            onEnter: () => resetRoomOfRequirementCount(),
            exits: [
                {
                    dir: 'stairs',
                    id: 'stairs'
                }
            ]
        },
        {
            id: '5thfloorhallways',
            name: '5th Floor Hallways',
            desc: `The 5th floor classrooms are empty but unlocked. Filch must be around cleaning somewhere. I should avoid him if I can.\nThe Prefect Bathrooms are locked, they have private keys to get in.`,
            onEnter: () => resetRoomOfRequirementCount(),
            exits: [
                {
                    dir: 'stairs',
                    id: 'stairs'
                }
            ]
        },
        {
            id: '6thfloorhallways',
            name: '6th Floor Hallways',
            desc: `The 6th floor classrooms are empty and locked. There is nothing of interest here. Why is this even in the game?`,
            onEnter: () => resetRoomOfRequirementCount(),
            exits: [
                {
                    dir: 'stairs',
                    id: 'stairs'
                }
            ]
        },
        {
            id: '7thfloorhallways',
            name: '7th Floor Hallways',
            desc: `There are some common study rooms here with nothing of interest. The Gryffindor and Ravenclaw Common Rooms are here too, but I have no reason to go into a common room right now.\nBesides, I forgot the password and have no time for riddles right now. Professor Snape needs me and I'd rather not get on the "Harry" side of the professor.`,
            onEnter: () => {
                const count = incrementRoomOfRequirementCount();
                if (count >= 3) {
                    println(`***You unlocked the Room of Requirement and ingredients spill out into the hallway as the room closes itself.***\n\n**But at what cost..?**`);
                    const room = getRoom('7thfloorhallways');
                    room.items.push(
                        {
                            name: 'crushed snake fangs',
                            isTakeable: true,
                            onUse: () => {
                                const room = getRoom(disk.roomId);
                                if (room.id === 'potionsclassroom') {
                                    println("You add the crushed snake fangs to the cauldron.");
                                    addToPotion('crushed snake fangs');
                                }
                                else {
                                    println("I need to be in the Potions Classroom to add this to the cauldron.");
                                }
                            }
                        }
                    );
                    room.items.push(
                        {
                            name: 'sliced pungous onions',
                            isTakeable: true,
                            onUse: () => {
                                const room = getRoom(disk.roomId);
                                if (room.id === 'potionsclassroom') {
                                    println('You add the sliced pungous onions to the cauldron.');
                                    addToPotion('sliced pungous onions');
                                }
                                else {
                                    println("I need to be in the Potions Classroom to add this to the cauldron.");
                                }
                            }
                        }
                    );
                    room.items.push(
                        {
                            name: 'dried nettles',
                            isTakeable: true,
                            onUse: () => {
                                const room = getRoom(disk.roomId);
                                if (room.id === 'potionsclassroom') {
                                    println('You add the dried nettles to the cauldron.');
                                    addToPotion('dried nettles');
                                }
                                else {
                                    println("I need to be in the Potions Classroom to add this to the cauldron.");
                                }
                            }
                        }
                    );
                    room.items.push(
                        {
                            name: 'flobberworm mucus',
                            isTakeable: true,
                            onUse: () => {
                                const room = getRoom(disk.roomId);
                                if (room.id === 'potionsclassroom') {
                                    println('You add a dash of flobberworm mucus to the cauldron.');
                                    addToPotion('flobberworm mucus');
                                }
                                else {
                                    println("I need to be in the Potions Classroom to add this to the cauldron.");
                                }
                            }
                        }
                    );
                    room.items.push(
                        {
                            name: 'powdered ginger root',
                            isTakeable: true,
                            onUse: () => {
                                const room = getRoom(disk.roomId);
                                if (room.id === 'potionsclassroom') {
                                    println('You add a sprinkle of powdered ginger root to the cauldron.');
                                    addToPotion('powdered ginger root');
                                }
                                else {
                                    println("I need to be in the Potions Classroom to add this to the cauldron.");
                                }
                            }
                        }
                    );
                    room.items.push(
                        {
                            name: 'pickled shrake spines',
                            isTakeable: true,
                            onUse: () => {
                                const room = getRoom(disk.roomId);
                                if (room.id === 'potionsclassroom') {
                                    println('You add the pickled shrake spines to the cauldron.');
                                    addToPotion('pickled shrake spines');
                                }
                                else {
                                    println("I need to be in the Potions Classroom to add this to the cauldron.");
                                }
                            }
                        }
                    );
                    room.items.push(
                        {
                            name: 'stewed horned slugs',
                            isTakeable: true,
                            onUse: () => {
                                const room = getRoom(disk.roomId);
                                if (room.id === 'potionsclassroom') {
                                    println('You add a glug of stewed horned slugs to the cauldron.');
                                    addToPotion('stewed horned slugs');
                                }
                                else {
                                    println("I need to be in the Potions Classroom to add this to the cauldron.");
                                }
                            }
                        }
                    );
                    room.items.push(
                        {
                            name: 'porcupine quills',
                            isTakeable: true,
                            onUse: () => {
                                const room = getRoom(disk.roomId);
                                if (room.id === 'potionsclassroom') {
                                    println('You add the porcupine quills to the cauldron.');
                                    addToPotion('porcupine quills');
                                }
                                else {
                                    println("I need to be in the Potions Classroom to add this to the cauldron.");
                                }
                            }
                        }
                    );
                }
            },
            items: [],
            exits: [
                {
                    dir: 'stairs',
                    id: 'stairs'
                }
            ] 
        }
    ],
    inventory: [
        {
            name: 'wand',
            desc: "It's the wand that chose you when you started at Hogwarts.",
            onUse: () => {
                const room = getRoom(disk.roomId);
                if (room.id === 'potionsclassroom') {
                    println('You wave your wand over the cauldron with confident precision and accuracy.');
                    addToPotion('wand');
                }
                else {
                    println("I can use this in the Potions Classroom when the recipe calls for it.");
                }
            }
        }
    ]
});




// global properties, assigned with let for easy overriding by the user
let diskFactory;
let disk;

// store user input history
let inputs = [];
let inputsPos = 0;

// records the input string for saving and loading while using a link action
// do not record inputs from loading a game
// string -> nothing
let recordInput = (input) => {
    if (!isLoading) {
        inputs.push(input);
        inputsPos++;
    }
};

// define list style
let bullet = '•';

// queue output for improved performance
let printQueue = [];

// reference to the input element
let input = document.querySelector('#input');

// add any default values to the disk
// disk -> disk
let init = (disk) => {
    const initializedDisk = Object.assign({}, disk);
    initializedDisk.rooms = disk.rooms.map((room) => {
        // number of times a room has been visited
        room.visits = 0;
        return room;
    });

    if (!initializedDisk.inventory) {
        initializedDisk.inventory = [];
    }

    if (!initializedDisk.characters) {
        initializedDisk.characters = [];
    }

    initializedDisk.characters = initializedDisk.characters.map(char => {
        // player's conversation history with this character
        char.chatLog = [];
        return char;
    });

    return initializedDisk;
};

// register listeners for input events
let setup = () => {
    input.addEventListener('keypress', (e) => {
        const ENTER = 13;

        if (e.keyCode === ENTER) {
            applyInput();
        }
    });

    input.addEventListener('keydown', (e) => {
        input.focus();

        const UP = 38;
        const DOWN = 40;
        const TAB = 9;

        if (e.keyCode === UP) {
            navigateHistory('prev');
        } else if (e.keyCode === DOWN) {
            navigateHistory('next');
        } else if (e.keyCode === TAB) {
            e.stopPropagation();
            e.preventDefault()
            autocomplete();
        }
    });

    input.addEventListener('focusout', () => {
        input.focus({ preventScroll: true });
    });
};

// store player input history
// (optionally accepts a name for the save)
let save = (name = 'save') => {
    localStorage.setItem(name, JSON.stringify(inputs));
    const line = name.length ? `Game saved as "${name}".` : `Game saved.`;
    println(line);
};

// reapply inputs from saved game
// (optionally accepts a name for the save)
let load = (name = 'save') => {
    let save = localStorage.getItem(name);

    if (!save) {
        println(`Save file not found.`);
        return;
    }

    // if the disk provided is an object rather than a factory function, the game state must be reset by reloading
    if (typeof diskFactory !== 'function' && inputs.length) {
        println(`You cannot load this disk in the middle of the game. Please reload the browser, then run the **LOAD** command again.`);
        return;
    }

    inputs = [];
    inputsPos = 0;
    loadDisk();

    applyInputs(save);

    const line = name.length ? `Game "${name}" was loaded.` : `Game loaded.`;
    println(line);
    renderUi();
};

// export current game to disk (optionally accepts a filename)
let exportSave = (name) => {
    const filename = `${name.length ? name : 'text-engine-save'}.txt`;
    saveFile(JSON.stringify(inputs), filename);
    println(`Game exported to "${filename}".`);
};

// import a previously exported game from disk
let importSave = () => {
    // if the disk provided is an object rather than a factory function, the game state must be reset by reloading
    if (typeof diskFactory !== 'function' && inputs.length) {
        println(`You cannot load this disk in the middle of the game. Please reload the browser, then run the **LOAD** command again.`);
        return;
    }

    const input = openFile();
    input.onchange = () => {
        const fr = new FileReader();
        const file = input.files[0];

        // register file loaded callback
        fr.onload = () => {
            // load the game
            inputs = [];
            inputsPos = 0;
            loadDisk();
            applyInputs(fr.result);
            println(`Game "${file.name}" was loaded.`);
            input.remove();
            renderUi();
        };

        // register error handling
        fr.onerror = (event) => {
            println(`An error occured loading ${file.name}. See console for more information.`);
            console.error(`Reader error: ${fr.error}
        Reader error event: ${event}`);
            input.remove();
        };

        // attempt to load the text from the selected file
        fr.readAsText(file);
    };
};

// saves text from memory to disk
let saveFile = (content, filename) => {
    const a = document.createElement('a');
    const file = new Blob([content], { type: 'text/plain' });

    a.href = URL.createObjectURL(file);
    a.download = filename;
    a.click();

    URL.revokeObjectURL(a.href);
};

// creates input element to open file prompt (allows user to load exported game from disk)
let openFile = () => {
    const input = document.createElement('input');
    input.setAttribute('type', 'file');
    input.click();

    return input;
};

let isLoading = false;
// applies string representing an array of input strings (used for loading saved games)
let applyInputs = (string) => {
    isLoading = true;
    let ins = [];

    try {
        ins = JSON.parse(string);
    } catch (err) {
        println(`An error occurred. See error console for more details.`);
        console.error(`An error occurred while attempting to parse text-engine inputs.
      Inputs: ${string}
      Error: ${err}`);
        return;
    }

    while (ins.length) {
        applyInput(ins.shift());
    }
    isLoading = false;
};

// increment the room of requirement count and return the new number
// nothing -> number
let incrementRoomOfRequirementCount = () => ++disk.roomofrequirementcount;

// reset the room of requirement count
// nothing -> nothing
let resetRoomOfRequirementCount = () => disk.roomofrequirementcount = 0;

// add ingredient to Cure for Boils potion
// string -> nothing
let addToPotion = (ingredient) => {
    const correctPotion = [
        'crushed snake fangs',
        'stir',
        'sliced pungous onions',
        'burner is on',
        'dried nettles',
        'flobberworm mucus',
        'vigorous stir',
        'powdered ginger root',
        'vigorous stir',
        'pickled shrake spines',
        'gentle stir',
        'stewed horned slugs',
        'burner is off',
        'porcupine quills',
        'wand'
    ];

    // check if they are correct so far
    for (let i = 0; i < disk.potion.length; i++) {
        if (!disk.potion[i].includes(correctPotion[i])) {
            println(`Stop trying. It's over. **LOAD** a previous save or start over for a chance at unlocking the secret menu.`);
            return;
        }
    }

    disk.potion.push(ingredient);

    // if they completed the potion, then they win!
    if (disk.potion.length === 15 && ingredient === 'wand') {
        // TODO Unlock secret menu
        println(`***YOU WIN!\nCongrats on unlocking the secret menu!\nIf you don't have access, please promptly yell at a developer via Howler.***`);
    }

    switch (disk.potion.length) {
        case 1:
            if (ingredient !== 'crushed snake fangs') {
                println(`SNAPE: "Hmm.. interesting. You can't even get the first step right. Leave my classroom at once. I apologize for placing my faith in you."\n*(**LOAD** your previous save or start over. Longbottom is dead in this timeline because of your actions. Thankfully you have a time-turner to do better next time. You did remember to **SAVE**.. right? Because that would be most unfortunate.)*`);
            }
            break;
        case 2:
            if (ingredient === 'burner is on') {
                println(`SNAPE: "Remind me, which book did I tell you to follow? The Book of Potions or that second rate copy of Magical Drafts and Potions? Get out of my classroom."\n*(**LOAD** your previous save or start over. Longbottom is dead in this timeline because of your actions. Thankfully you have a time-turner to do better next time. You did remember to **SAVE**.. right? Because that would be most unfortunate.)*`);
            }    
            else if (ingredient !== 'gentle stir' && ingredient !== 'vigorous stir') {
                println(`SNAPE: "Did you forget how to read? You were supposed to STIR the potion you idiot. Get out of my classroom at once."\n*(**LOAD** your previous save or start over. Longbottom is dead in this timeline because of your actions. Thankfully you have a time-turner to do better next time. You did remember to **SAVE**.. right? Because that would be most unfortunate.)*`);
            }
            break;
        case 3:
            if (ingredient !== 'sliced pungous onions') {
                println(`SNAPE: "Hmm, that doesn't smell like pungous onions. Looks like you ruined the potion." *Snape begins a vicious slow clap with a low grin on his face.* "You may now leave my classroom."\n*(**LOAD** your previous save or start over. Longbottom is dead in this timeline because of your actions. Thankfully you have a time-turner to do better next time. You did remember to **SAVE**.. right? Because that would be most unfortunate.)*`);
            }
            break;
        case 4:
            if (ingredient !== 'burner is on') {
                println(`SNAPE: "Shame. Such a simple step of turning on the burner and you couldn't handle it. What? Afraid of the heat? Get out of my sight."\n*(**LOAD** your previous save or start over. Longbottom is dead in this timeline because of your actions. Thankfully you have a time-turner to do better next time. You did remember to **SAVE**.. right? Because that would be most unfortunate.)*`);
            }
            break;
        case 5:
            if (ingredient !== 'dried nettles') {
                println(`SNAPE: "That was incorrect. Leave my classroom and I would advise you to not enroll in my class next semester. It would be a waste of my time."\n*(**LOAD** your previous save or start over. Longbottom is dead in this timeline because of your actions. Thankfully you have a time-turner to do better next time. You did remember to **SAVE**.. right? Because that would be most unfortunate.)*`);
            }
            break;
        case 6:
            if (ingredient === 'whole flobberworm') {
                println(`SNAPE: "Remarkable. Not only can't you read, but you stole unnecessary ingredients and proceded to ruin them. Read it again. Did the book tell you to add a whole flobberworm to the potion?\nI thought not. Return to your common room and do not come back."\n*(**LOAD** your previous save or start over. Longbottom is dead in this timeline because of your actions. Thankfully you have a time-turner to do better next time. You did remember to **SAVE**.. right? Because that would be most unfortunate.)*`);
            }
            else if (ingredient !== 'flobberworm mucus') {
                println(`SNAPE: "Interesting. Do you always ignore the guidance of those above you? Seeing where you are, you are in no position to question what anyone directs you to do. And I direct you to learn how to read and to leave my classroom."\n*(**LOAD** your previous save or start over. Longbottom is dead in this timeline because of your actions. Thankfully you have a time-turner to do better next time. You did remember to **SAVE**.. right? Because that would be most unfortunate.)*`);
            }
            break;
        case 7:
            if (ingredient !== 'vigorous stir') {
                println(`SNAPE: "That didn't look like a vigorous stir to me. Do you want to end up next to Longbottom? Because you were very close. I can't have hazards like you roaming my classroom. Leave."\n*(**LOAD** your previous save or start over. Longbottom is dead in this timeline because of your actions. Thankfully you have a time-turner to do better next time. You did remember to **SAVE**.. right? Because that would be most unfortunate.)*`);
            }
            break;
        case 8:
            if (ingredient !== 'powdered ginger root') {
                println(`SNAPE: "You're barely half-way through the recipe and this is how you choose to mess it up? Well done. This will be noted for our future classes together. You may leave now."\n*(**LOAD** your previous save or start over. Longbottom is dead in this timeline because of your actions. Thankfully you have a time-turner to do better next time. You did remember to **SAVE**.. right? Because that would be most unfortunate.)*`);
            }
            break;
        case 9:
            if (ingredient !== 'vigorous stir') {
                println(`SNAPE: "A failure to comprehend the simplest instructions. What a disgusting waste of magical blood. Leave me."\n*(**LOAD** your previous save or start over. Longbottom is dead in this timeline because of your actions. Thankfully you have a time-turner to do better next time. You did remember to **SAVE**.. right? Because that would be most unfortunate.)*`);
            }
            break;
        case 10:
            if (ingredient !== 'pickled shrake spines') {
                println(`SNAPE: "Shame. And here I thought you were almost as good as Malfoy. But here we are. Get out before you embarrass yourself further."\n*(**LOAD** your previous save or start over. Longbottom is dead in this timeline because of your actions. Thankfully you have a time-turner to do better next time. You did remember to **SAVE**.. right? Because that would be most unfortunate.)*`);
            }
            break;
        case 11:
            if (ingredient !== 'gentle stir') {
                println(`SNAPE: "Did you forget how to read? The book says to gently stir after adding the pickled shrake spines as to not overexcite them. All of these fine ingredients, wasted. Leave before you cause more distruction."\n*(**LOAD** your previous save or start over. Longbottom is dead in this timeline because of your actions. Thankfully you have a time-turner to do better next time. You did remember to **SAVE**.. right? Because that would be most unfortunate.)*`);
            }
            break;
        case 12:
            if (ingredient !== 'stewed horned slugs') {
                println(`SNAPE: "Are you too good for Hagrid's stewed horned slugs? Are they forgettable to you? Hagrid raises the finest horned slugs the magical world has ever seen and you choose to leave them out of the potion? You're an insult to the wizard name. Go back to your dorm and draft an apology letter to Hagrid at once."\n*(**In true Mike form, we're not fucking kidding. Go write one as a Howler that clearly identifies yourself as the sender. Tell Hagrid how good his stewed horned slugs are. Your secret menu is now manually locked until you do this and the Howler is read.**)*\n*(**LOAD** your previous save or start over. Longbottom is dead in this timeline because of your actions. Thankfully you have a time-turner to do better next time. You did remember to **SAVE**.. right? Because that would be most unfortunate.)*`);
                // This is a bluff, we don't have a way to automatically lock them out, but they don't know that. Lets see if anyone hits this.
            }
            break;
        case 13:
            if (ingredient === 'porcupine quills' && getBurnerState() === 'burner is on') {
                println(`*Snape laughs as the potion explodes in your face.*\nSNAPE: "You're as much of a fool as Longbottom is. I didn't think I would see this twice in the same day. Report your boil-ridden face to the Hospital Wing at once."\n*(**LOAD** your previous save or start over. Longbottom is dead in this timeline because of your actions and it looks like you'll soon join him. Thankfully you have a time-turner to do better next time. You did remember to **SAVE**.. right? Because that would be most unfortunate.)*`);
            }
            else if (ingredient !== 'burner is off') {
                println(`SNAPE: "Smells like something is burning. You forgot to turn off the burner. Typical mistake for someone of your caliber. Leave me while I correct your mistakes."\n*(**LOAD** your previous save or start over. Longbottom is dead in this timeline because of your actions. Thankfully you have a time-turner to do better next time. You did remember to **SAVE**.. right? Because that would be most unfortunate.)*`);
            }
            break;
        case 14:
            if (ingredient !== 'porcupine quills') {
                println(`SNAPE: "A mistake that only you could make. You were so close, yet you choose to ruin it at the last moment. A true indication of your future prospects. Leave my classroom."\n*(**LOAD** your previous save or start over. Longbottom is dead in this timeline because of your actions. Thankfully you have a time-turner to do better next time. You did remember to **SAVE**.. right? Because that would be most unfortunate.)*`);
            }
            break;
        case 15:
            if (ingredient !== 'wand') {
                println(`*Snape gives you a sly smile.*\nSNAPE: "This was the last step, wasn't it? Are you aware of how close you were to correctly brewing such an elementary potion? Are you aware of how many ingredients you just wasted because you failed to comprehend the simplist of instructions of this entire recipe? All you needed to do was wave your wand over the cauldron. Something a 2 year old could do, yet you couldn't. Leave this classroom and go ask Filch how to use a wand."\n*(**LOAD** your previous save or start over. Longbottom is dead in this timeline because of your actions. Thankfully you have a time-turner to do better next time. You did remember to **SAVE**.. right? Because that would be most unfortunate.)*`);
            }
            break;
    }
};

// toggles the burner and returns the new state as a string
// nothing -> string
let toggleBurner = () => {
    disk.burnerOn = !disk.burnerOn;
    return disk.burnerOn ? 'burner is on' : 'burner is off';
};

// returns the current state of the burner as a string
// nothing -> string
let getBurnerState = () => {
    return disk.burnerOn ? 'burner is on' : 'burner is off';
};

// set player's house
// string -> nothing
let setHouse = (newHouse) => disk.house = newHouse;

// get player's house
// nothing -> string
let getHouse = () => disk.house;

// list player inventory
let inv = () => {
    const items = disk.inventory.filter(item => !item.isHidden);

    if (!items.length) {
        println(`You don't have any items in your inventory.`);
        return;
    }

    println(`You have the following items in your inventory:`);
    items.forEach(item => renderItemWithLinks(item, false));
    renderUi();
};

// show room description
let look = () => {
    const room = getRoom(disk.roomId);

    if (typeof room.onLook === 'function') {
        room.onLook({ disk, println });
    }

    println(room.desc)
};

// look in the passed way
// string -> nothing
let lookThusly = (str) => println(`You look ${str}.`);

// look at the passed item or character
// array -> nothing
let lookAt = (args) => {
    let name = args;
    const item = getItemInInventory(name) || getItemInRoom(name, disk.roomId);

    if (item) {
        // Look at an item.
        if (item.desc) {
            println(item.desc);
        } else {
            println(`You don\'t notice anything remarkable about it.`);
        }

        if (typeof (item.onLook) === 'function') {
            item.onLook({ disk, println, getRoom, enterRoom, item });
        }
    } else {
        const character = getCharacter(name, getCharactersInRoom(disk.roomId));
        if (character) {
            // Look at a character.
            if (character.desc) {
                println(character.desc);
            } else {
                println(`You don't notice anything remarkable about them.`);
            }

            if (typeof (character.onLook) === 'function') {
                character.onLook({ disk, println, getRoom, enterRoom, item });
            }
        } else {
            println(`You don't see any such thing.`);
        }
    }
    recordInput(`look ${args}`);
};

// displays the direction as a link to go to it
// string, string -> nothing
let renderDirectionWithLinks = (dir, directionName) => {
    println(`${bullet} <a href='#' onclick='goDir("${dir}");'>${directionName}</a>`);
};

// list available exits
let go = () => {
    const room = getRoom(disk.roomId);
    const exits = room.exits.filter(exit => !exit.isHidden);

    if (!exits) {
        println(`There's nowhere to go.`);
        return;
    }

    println(`Where would you like to go from here?`);
    exits.forEach((exit) => {
        const rm = getRoom(exit.id);

        if (!rm) {
            return;
        }

        const dir = getName(exit.dir).toUpperCase();
        // include room name if player has been there before
        const directionName = rm.visits > 0
            ? `${dir} - ${rm.name}`
            : dir

        renderDirectionWithLinks(dir, directionName);
    });
    renderUi();
};

// find the exit with the passed direction in the given list
// string, array -> exit
let getExit = (dir, exits) => exits.find(exit =>
    Array.isArray(exit.dir)
        ? exit.dir.includes(dir)
        : exit.dir.toLowerCase() === dir.toLowerCase()
);

// shortcuts for cardinal directions
// (allows player to type e.g. 'go n')
let shortcuts = {
    n: 'north',
    s: 'south',
    e: 'east',
    w: 'west',
    ne: 'northeast',
    nw: 'northwest',
    se: 'southeast',
    sw: 'southwest',
};

// go the passed direction
// string -> nothing
let goDir = (dir) => {
    const room = getRoom(disk.roomId);
    const exits = room.exits;

    if (!exits) {
        println(`There's nowhere to go.`);
        return;
    }

    const nextRoom = getExit(dir, exits);

    if (!nextRoom) {
        // check if the dir is a shortcut
        if (shortcuts[dir]) {
            goDir(shortcuts[dir]);
        } else {
            println(`There is no exit in that direction.`);
        }
        return;
    }

    if (nextRoom.block) {
        println(nextRoom.block);
        return;
    }

    recordInput(`go ${dir}`);
    enterRoom(nextRoom.id);
};

// shortcuts for cardinal directions
// (allows player to type just e.g. 'n')
let n = () => goDir('north');
let s = () => goDir('south');
let e = () => goDir('east');
let w = () => goDir('west');
let ne = () => goDir('northeast');
let se = () => goDir('southeast');
let nw = () => goDir('northwest');
let sw = () => goDir('southwest');

// if there is one character in the room, engage that character in conversation
// otherwise, list characters in the room
let talk = () => {
    const characters = getCharactersInRoom(disk.roomId);

    // assume players wants to talk to the only character in the room
    if (characters.length === 1) {
        talkToOrAboutX('to', getName(characters[0].name));
        return;
    }

    // list characters in the room
    println(`You can talk TO someone or ABOUT some topic.`);
    chars();
};

// speak to someone or about some topic
// string, string -> nothing
let talkToOrAboutX = (preposition, x) => {
    const room = getRoom(disk.roomId);

    if (preposition !== 'to' && preposition !== 'about') {
        println(`You can talk TO someone or ABOUT some topic.`);
        return;
    }

    const character =
        preposition === 'to' && getCharacter(x, getCharactersInRoom(room.id))
            ? getCharacter(x, getCharactersInRoom(room.id))
            : disk.conversant;
    let topics;

    // give the player a list of topics to choose from for the character
    const listTopics = () => {
        // capture reference to the current conversation
        disk.conversation = topics;

        if (topics.length) {
            const availableTopics = topics.filter(topic => topicIsAvailable(character, topic));

            if (availableTopics.length) {
                availableTopics.forEach(topic => println(`${bullet} ${topic.option ? topic.option : topic.keyword.toUpperCase()}`));
                println(`${bullet} **BYE**`);
            } else {
                // if character isn't handling onTalk, let the player know they are out of topics
                if (!character.onTalk) {
                    println(`You have nothing to discuss with ${getName(character.name)} at this time.`);
                }
                endConversation();
            }
        } else if (Object.keys(topics).length) {
            println(`Select a response:`);
            Object.keys(topics).forEach(topic => println(`${bullet} ${topics[topic].option}`));
        } else {
            endConversation();
        }
    };

    if (preposition === 'to') {
        if (!getCharacter(x)) {
            println(`There is no one here by that name.`);
            return;
        }

        if (!getCharacter(getName(x), getCharactersInRoom(room.id))) {
            println(`There is no one here by that name.`);
            return;
        }

        if (!character.topics) {
            println(`You have nothing to discuss with ${getName(character.name)} at this time.`);
            return;
        }

        if (typeof (character.topics) === 'string') {
            println(character.topics);
            return;
        }

        if (typeof (character.onTalk) === 'function') {
            character.onTalk({ disk, println, getRoom, enterRoom, room, character });
        }

        topics = typeof character.topics === 'function'
            ? character.topics({ println, room })
            : character.topics;

        if (!topics.length && !Object.keys(topics).length) {
            println(`You have nothing to discuss with ${getName(character.name)} at this time.`);
            return;
        }

        // initialize the chat log if there isn't one yet
        character.chatLog = character.chatLog || [];
        disk.conversant = character;
        listTopics(topics);
    } else if (preposition === 'about') {
        if (!disk.conversant) {
            println(`You need to be in a conversation to talk about something.`);
            return;
        }
        const character = eval(disk.conversant);
        if (getCharactersInRoom(room.id).includes(disk.conversant)) {
            const response = x.toLowerCase();
            if (response === 'bye') {
                endConversation();
                println(`You end the conversation.`);
            } else if (disk.conversation && disk.conversation[response]) {
                disk.conversation[response].onSelected();
            } else {
                const topic = disk.conversation.length && conversationIncludesTopic(disk.conversation, response);
                const isAvailable = topic && topicIsAvailable(character, topic);
                if (isAvailable) {
                    if (topic.line) {
                        println(topic.line);
                    }
                    if (topic.onSelected) {
                        topic.onSelected({ disk, println, getRoom, enterRoom, room, character });
                    }
                    // add the topic to the log
                    character.chatLog.push(getKeywordFromTopic(topic));
                } else {
                    println(`You talk about ${removePunctuation(x)}.`);
                    println(`Type the capitalized KEYWORD to select a topic.`);
                }
            }

            // continue the conversation.
            if (disk.conversation) {
                topics = typeof character.topics === 'function'
                    ? character.topics({ println, room })
                    : character.topics;
                listTopics(character);
            }
        } else {
            println(`That person is no longer available for conversation.`);
            disk.conversant = undefined;
            disk.conversation = undefined;
        }
    }
};

// list takeable items in room
let take = () => {
    const room = getRoom(disk.roomId);
    const items = (room.items || []).filter(item => item.isTakeable && !item.isHidden);

    if (!items.length) {
        println(`There's nothing to take.`);
        return;
    }

    println(`The following items can be taken:`);
    items.forEach(item => println(`${bullet} ${getName(item.name)}`));
};

// take the item with the given name
// string -> nothing
let takeItem = (itemName) => {
    const room = getRoom(disk.roomId);
    const findItem = item => objectHasName(item, itemName);
    let itemIndex = room.items && room.items.findIndex(findItem);

    if (typeof itemIndex === 'number' && itemIndex > -1) {
        const item = room.items[itemIndex];
        if (item.isTakeable) {
            disk.inventory.push(item);
            room.items.splice(itemIndex, 1);

            if (typeof item.onTake === 'function') {
                item.onTake({ disk, println, room, getRoom, enterRoom, item });
            } else {
                println(`You took the ${getName(item.name)}.`);
            }
        } else {
            if (typeof item.onTake === 'function') {
                item.onTake({ disk, println, room, getRoom, enterRoom, item });
            } else {
                println(item.block || `You can't take that.`);
            }
        }
    } else {
        itemIndex = disk.inventory.findIndex(findItem);
        if (typeof itemIndex === 'number' && itemIndex > -1) {
            println(`You already have that.`);
        } else {
            println(`You don't see any such thing.`);
        }
    }
    recordInput(`take ${itemName}`);
};

// list useable items in room and inventory
let use = () => {
    const room = getRoom(disk.roomId);

    const useableItems = (room.items || [])
        .concat(disk.inventory)
        .filter(item => item.onUse && !item.isHidden);

    if (!useableItems.length) {
        println(`There's nothing to use.`);
        return;
    }

    println(`The following items can be used:`);
    useableItems.forEach((item) => {
        println(`${bullet} ${getName(item.name)}`)
    });
};

// use the item with the given name
// string -> nothing
let useItem = (itemName) => {
    const item = getItemInInventory(itemName) || getItemInRoom(itemName, disk.roomId);

    if (!item) {
        println(`You don't have that.`);
        return;
    }

    if (item.use) {
        console.warn(`Warning: The "use" property for Items has been renamed to "onUse" and support for "use" has been deprecated in text-engine 2.0. Please update your disk, renaming any "use" methods to be called "onUse" instead.`);

        item.onUse = item.use;
    }

    if (!item.onUse) {
        println(`This item doesn't have a use.`);
        return;
    }

    // use item and give it a reference to the game
    if (typeof item.onUse === 'string') {
        const use = eval(item.onUse);
        use({ disk, println, getRoom, enterRoom, item });
    } else if (typeof item.onUse === 'function') {
        item.onUse({ disk, println, getRoom, enterRoom, item });
    }
    recordInput(`use ${itemName}`);
};

// displays item with use, look at, and take (if is takeable) click actions
// string -> nothing
let renderItemWithLinks = (item, isTakeable) => {
    const name = getName(item.name);
    const display = [`${bullet} ${name}`];
    display.push(`(<a href='#' onclick='useItem("${name}");'>use</a>)`);
    display.push(`(<a href='#' onclick='lookAt("${name}");'>look at</a>)`);
    if (isTakeable) {
        display.push(`(<a href='#' onclick='takeItem("${name}");'>take</a>)`);
    }
    println(`${display.join(' ')}`);
};

// list items in room
let items = () => {
    const room = getRoom(disk.roomId);
    const items = (room.items || []).filter(item => !item.isHidden);

    if (!items.length) {
        println(`There's nothing here.`);
        return;
    }

    println(`You find the following:`);
    items
        .forEach(item => renderItemWithLinks(item, true));
    renderUi();
};

// list characters in room
let chars = () => {
    const room = getRoom(disk.roomId);
    const chars = getCharactersInRoom(room.id).filter(char => !char.isHidden)

    if (!chars.length) {
        println(`There's no one here.`);
        return;
    }

    println(`You see the following:`);
    chars
        .forEach(char => println(`${bullet} ${getName(char.name)}`));
};

// display help menu
let help = () => {
    const instructions = `The following commands are available:
    **LOOK**:           look at the room
    **LOOK**:           'look key'
    **TAKE**:           'take book'
    **GO**:             'go north'
    **USE**:            'use door'
    **TALK TO**:           'talk to mary'
    **ITEMS**:          list items in the room
    **CHARS**:          list characters in the room
    **INV**:            list inventory items
    **SAVE**/**LOAD**:      save current game, or load a saved game
    **HELP**:   this help menu
  `;
    println(instructions);
};

let lumos = () => println("Just kidding, we didn't implement that yet.");
let reparo = () => println("This isn't implemented, but it was a nice thought.");

// handle say command with no args
let say = () => println([`Say what?`, `You don't say.`]);

// say the passed string
// string -> nothing
let sayString = (str) => println(`You say ${removePunctuation(str)}.`);

// retrieve user input (remove whitespace at beginning or end)
// nothing -> string
let getInput = () => input.value.trim();

// objects with methods for handling commands
// the array should be ordered by increasing number of accepted parameters
// e.g. index 0 means no parameters ("help"), index 1 means 1 parameter ("go north"), etc.
// the methods should be named after the command (the first argument, e.g. "help" or "go")
// any command accepting multiple parameters should take in a single array of parameters
// if the user has entered more arguments than the highest number you've defined here, we'll use the last set
let commands = [
    // no arguments (e.g. "help", "chars", "inv")
    {
        inv,
        i: inv, // shortcut for inventory
        inventory: inv,
        look,
        l: look, // shortcut for look
        go,
        n,
        s,
        e,
        w,
        ne,
        se,
        sw,
        nw,
        talk,
        t: talk, // shortcut for talk
        take,
        get: take,
        items,
        use,
        chars,
        characters: chars,
        help,
        say,
        save,
        load,
        restore: load,
        lumos,
        reparo
    },
    // one argument (e.g. "go north", "take book")
    {
        look: lookAt,
        go: goDir,
        take: takeItem,
        get: takeItem,
        use: useItem,
        say: sayString,
        save: x => save(x),
        load: x => load(x),
        restore: x => load(x),
        x: x => lookAt(x), // IF standard shortcut for look at
        t: x => talkToOrAboutX('to', x), // IF standard shortcut for talk
        export: exportSave,
        import: importSave, // (ignores the argument)
    },
    // two+ arguments (e.g. "look at key", "talk to mary")
    {
        say(args) {
            const str = args.reduce((cur, acc) => cur + ' ' + acc, '');
            sayString(str);
        },
        talk: args => talkToOrAboutX(args[0], args[1])
    },
];

// process user input & update game state (bulk of the engine)
// accepts optional string input; otherwise grabs it from the input element
let applyInput = (input) => {
    let isNotSaveLoad = (cmd) => !cmd.toLowerCase().startsWith('save')
        && !cmd.toLowerCase().startsWith('load')
        && !cmd.toLowerCase().startsWith('export')
        && !cmd.toLowerCase().startsWith('import');

    input = input || getInput();
    inputs.push(input);
    inputs = inputs.filter(isNotSaveLoad);
    inputsPos = inputs.length;
    println(`> ${input}`);

    const val = input.toLowerCase();
    setInput(''); // reset input field

    const exec = (cmd, arg) => {
        if (cmd) {
            cmd(arg);
        } else if (disk.conversation) {
            println(`Type the capitalized KEYWORD to select a topic.`);
        } else {
            println(`Sorry, I didn't understand your input. For a list of available commands, type HELP.`);
        }
    };

    let values = val.split(' ')

    // remove articles
    // (except for the say command, which prints back what the user said)
    // (and except for meta commands to allow save names such as 'a')
    if (values[0] !== 'say' && isNotSaveLoad(values[0])) {
        values = values.filter(arg => arg !== 'a' && arg !== 'an' && arg != 'the');
    }

    const [command, ...args] = values;
    const room = getRoom(disk.roomId);

    if (args.length === 1) {
        exec(commands[1][command], args[0]);
    } else if (command === 'take' && args.length) {
        // support for taking items with spaces in the names
        // (just tries to match on the first word)
        takeItem(args.join(' '));
    } else if (command === 'use' && args.length) {
        // support for using items with spaces in the names
        // (just tries to match on the first word)
        useItem(args.join(' '));
    } else if (args.length >= commands.length) {
        exec(commands[commands.length - 1][command], args);
    } else if (room.exits && getExit(command, room.exits)) {
        // handle shorthand direction command, e.g. "EAST" instead of "GO EAST"
        goDir(command);
    } else if (disk.conversation && (disk.conversation[command] || conversationIncludesTopic(disk.conversation, command))) {
        talkToOrAboutX('about', command);
    } else {
        exec(commands[args.length][command], args);
    }
};

// allows wrapping text in special characters so println can convert them to HTML tags
// string, string, string -> string
let addStyleTags = (str, char, tagName) => {
    let odd = true;
    while (str.includes(char)) {
        const tag = odd ? `<${tagName}>` : `</${tagName}>`;
        str = str.replace(char, tag);
        odd = !odd;
    }

    return str;
};

// overwrite user input
// string -> nothing
let setInput = (str) => {
    input.value = str;
    // on the next frame, move the cursor to the end of the line
    setTimeout(() => {
        input.selectionStart = input.selectionEnd = input.value.length;
    });
};

// render output, with optional class
// (string | array | fn -> string) -> nothing
let println = (line, className) => {
    // bail if string is null or undefined
    if (!line) {
        return;
    }

    let str =
        // if this is an array of lines, pick one at random
        Array.isArray(line) ? pickOne(line)
            // if this is a method returning a string, evaluate it
            : typeof line === 'function' ? line()
                // otherwise, line should be a string
                : line;

    const output = document.querySelector('#output');
    const newLine = document.createElement('div');

    if (className) {
        newLine.classList.add(className);
    }

    // add a class for styling prior user input
    if (line[0] === '>') {
        newLine.classList.add('user');
    }

    // support for markdown-like bold, italic, underline & strikethrough tags
    if (className !== 'img') {
        str = addStyleTags(str, '__', 'u');
        str = addStyleTags(str, '**', 'b');
        str = addStyleTags(str, '*', 'i');
        str = addStyleTags(str, '~~', 'strike');
    }

    // maintain line breaks
    while (str.includes('\n')) {
        str = str.replace('\n', '<br>');
    }

    newLine.innerHTML = str;

    // push into the queue to print to the DOM
    printQueue.push(newLine);
};

// predict what the user is trying to type
let autocomplete = () => {
    const room = getRoom(disk.roomId);
    const words = input.value.toLowerCase().trim().split(/\s+/);
    const wordsSansStub = words.slice(0, words.length - 1);
    const itemNames = (room.items || []).concat(disk.inventory).map(item => item.name);

    const stub = words[words.length - 1];
    let options;

    if (words.length === 1) {
        // get the list of options from the commands array
        // (exclude one-character commands from auto-completion)
        const allCommands = commands
            .reduce((acc, cur) => acc.concat(Object.keys(cur)), [])
            .filter(cmd => cmd.length > 1);

        options = [...new Set(allCommands)];
        if (disk.conversation) {
            options = Array.isArray(disk.conversation)
                ? options.concat(disk.conversation.map(getKeywordFromTopic))
                : Object.keys(disk.conversation);
            options.push('bye');
        }
    } else if (words.length === 2) {
        const optionMap = {
            talk: ['to', 'about'],
            take: itemNames,
            use: itemNames,
            go: (room.exits || []).map(exit => exit.dir),
            look: ['at'],
        };
        options = optionMap[words[0]];
    } else if (words.length === 3) {
        const characterNames = (getCharactersInRoom(room.id) || []).map(character => character.name);
        const optionMap = {
            to: characterNames,
            at: characterNames.concat(itemNames),
        };
        options = (optionMap[words[1]] || []).flat().map(string => string.toLowerCase());
    }

    const stubRegex = new RegExp(`^${stub}`);
    const matches = (options || []).flat().filter(option => option.match(stubRegex));

    if (!matches.length) {
        return;
    }

    if (matches.length > 1) {
        const longestCommonStartingSubstring = (arr1) => {
            const arr = arr1.concat().sort();
            const a1 = arr[0];
            const a2 = arr[arr.length - 1];
            const L = a1.length;
            let i = 0;
            while (i < L && a1.charAt(i) === a2.charAt(i)) {
                i++;
            }
            return a1.substring(0, i);
        };

        input.value = [...wordsSansStub, longestCommonStartingSubstring(matches)].join(' ');
    } else {
        input.value = [...wordsSansStub, matches[0]].join(' ');
    }
};

// select previously entered commands
// string -> nothing
let navigateHistory = (dir) => {
    if (dir === 'prev') {
        inputsPos--;
        if (inputsPos < 0) {
            inputsPos = 0;
        }
    } else if (dir === 'next') {
        inputsPos++;
        if (inputsPos > inputs.length) {
            inputsPos = inputs.length;
        }
    }

    setInput(inputs[inputsPos] || '');
};

// get random array element
// array -> any
let pickOne = arr => arr[Math.floor(Math.random() * arr.length)];

// return the first name if it's an array, or the only name
// string | array -> string
let getName = name => typeof name === 'object' ? name[0] : name;

// retrieve room by its ID
// string -> room
let getRoom = (id) => disk.rooms.find(room => room.id === id);

// remove punctuation marks from a string
// string -> string
let removePunctuation = str => str.replace(/[.,\/#?!$%\^&\*;:{}=\_`~()]/g, "");

// remove extra whitespace from a string
// string -> string
let removeExtraSpaces = str => str.replace(/\s{2,}/g, " ");

// renders the UI
// nothing -> nothing
let renderUi = () => {
    const ui = [];
    ui.push(`<a href='#' onclick='inv();'>inventory</a>`);
    ui.push(`<a href='#' onclick='items();'>search</a>`);
    ui.push(`<a href='#' onclick='go();'>move</a>`);
    ui.push(`<a href='#' onclick='save();'>save</a>`);
    ui.push(`<a href='#' onclick='load();'>load</a>`);
    println(ui.join(' | '));
};

// move the player into room with passed ID
// string -> nothing
let enterRoom = (id) => {
    const room = getRoom(id);

    if (!room) {
        println(`That exit doesn't seem to go anywhere.`);
        return;
    }

    println(room.img, 'img');

    if (room.name) {
        println(`${getName(room.name)}`, 'room-name');
    }

    println(room.desc);

    room.visits++;

    disk.roomId = id;

    if (typeof room.onEnter === 'function') {
        room.onEnter({ disk, println, getRoom, enterRoom });
    }

    // reset any active conversation
    delete disk.conversation;
    delete disk.conversant;

    // display common controls
    go();
};

// determine whether the object has the passed name
// item | character, string -> bool
let objectHasName = (obj, name) => {
    const compareNames = n => n.toLowerCase() === name.toLowerCase();

    return Array.isArray(obj.name)
        ? obj.name.find(compareNames)
        : compareNames(obj.name);
}

// get a list of all characters in the passed room
// string -> characters
let getCharactersInRoom = (roomId) => disk.characters.filter(c => c.roomId === roomId);

// get a character by name from a list of characters
// string, characters -> character
let getCharacter = (name, chars = disk.characters) => chars.find(char => objectHasName(char, name));

// get item by name from room with ID
// string, string -> item
let getItemInRoom = (itemName, roomId) => {
    const room = getRoom(roomId);

    return room.items && room.items.find(item => objectHasName(item, itemName))
};

// get item by name from inventory
// string -> item
let getItemInInventory = (name) => disk.inventory.find(item => objectHasName(item, name));

// delete an item by name from inventory
// string -> nothing
let deleteItem = (itemName) => disk.inventory = disk.inventory.filter((x) => x.name !== itemName);

// get item by name
// string -> item
let getItem = (name) => getItemInInventory(name) || getItemInRoom(name, disk.roomId)

// retrieves a keyword from a topic
// topic -> string
let getKeywordFromTopic = (topic) => {
    if (topic.keyword) {
        return topic.keyword;
    }

    // find the keyword in the option (the word in all caps)
    const keyword = removeExtraSpaces(removePunctuation(topic.option))
        // separate words by spaces
        .split(' ')
        // find the word that is in uppercase
        // (must be at least 2 characters long)
        .find(w => w.length > 1 && w.toUpperCase() === w)
        .toLowerCase();

    return keyword;
};

// determine whether the passed conversation includes a topic with the passed keyword
// conversation, string -> boolean
let conversationIncludesTopic = (conversation, keyword) => {
    // BYE is always an option
    if (keyword === 'bye') {
        return true;
    }

    if (Array.isArray(disk.conversation)) {
        return disk.conversation.find(t => getKeywordFromTopic(t) === keyword);
    }

    return disk.conversation[keyword];
};

// determine whether the passed topic is available for discussion
// character, topic -> boolean
let topicIsAvailable = (character, topic) => {
    // topic has no prerequisites, or its prerequisites have been met
    const prereqsOk = !topic.prereqs || topic.prereqs.every(keyword => character.chatLog.includes(keyword));
    // topic is not removed after read, or it hasn't been read yet
    const readOk = !topic.removeOnRead || !character.chatLog.includes(getKeywordFromTopic(topic));

    return prereqsOk && readOk;
};

// end the current conversation
let endConversation = () => {
    disk.conversant = undefined;
    disk.conversation = undefined;
};

// load the passed disk and start the game
// disk -> nothing
let loadDisk = (uninitializedDisk) => {
    if (uninitializedDisk) {
        diskFactory = uninitializedDisk;
        // start listening for user input
        setup();
    }

    // initialize the disk
    // (although we expect the disk to be a factory function, we still support the old object format)
    disk = init(typeof diskFactory === 'function' ? diskFactory() : diskFactory);

    // start the game
    enterRoom(disk.roomId);

    // focus on the input
    input.focus();
};

// append any pending lines to the DOM each frame
let print = () => {
    if (printQueue.length) {
        while (printQueue.length) {
            output.appendChild(printQueue.shift());
        }

        // scroll to the most recent output at the bottom of the page
        window.scrollTo(0, document.body.scrollHeight);
    }

    requestAnimationFrame(print);
}

requestAnimationFrame(print);

// npm support
if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
    module.exports = loadDisk;
}

loadDisk(hpweekendDisk);
